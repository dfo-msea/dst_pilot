
# 2024 revisit:
# Switch from Gurobi to other solvers
# Update features that may have changed in Prioritizr
# Demonstrate functionality that Bea may need

library(prioritizr)
library(tidyverse)
library(scales)
library(tictoc)
library(glue)
library(sf)


########################################
# paths and data
########################################

root <- r'(C:\Users\cristianij\Documents\Projects\DST_pilot)'
gdb <- file.path(root, 'spatial/03_working/dst_grid.gdb')
fc <- 'dst_grid1km_PUVSP'
out_path <- file.path(root, 'scripts/dst_02_prioritizr/outputs')
targets_csv <- read_csv(file.path(root, 'scripts/dst_01_preprocess/DSTpilot_spatialData - naming_scheme.csv'))
gdb_mpa <- file.path(root, 'spatial/03_working/dst_mpas.gdb')
mpa_pu_fc <- 'mpas_rcas_marxan'
zoning <- read_csv(file.path(root, 'scripts/dst_02_prioritizr/Zoning frameworks - zones_features.csv'))


########################################
# pus, features, targets, mpas locked-in
########################################

# read in feature class containing Planning Units and area of each feature in each PU
pus <- st_read(gdb, layer = fc)

# features list
features <- names(pus)[grepl('eco', names(pus)) | grepl('hu', names(pus)) ]

# pus with MPAs to lock in
mpas <- st_read(gdb_mpa, layer=mpa_pu_fc)
mpas <- st_drop_geometry(mpas)
threshold <- 150000 # set a threshold of overlap so that a sliver doesn't get locked in
mpas <- mpas[mpas$Shape_Area > threshold, ]
mpas <- select(mpas, uID)
pus <- left_join(
  pus, 
  mpas, 
  by='uID', 
  keep=TRUE)
pus <- mutate(pus, lockedin = case_when(uID.y > 0 ~ TRUE, is.na(uID.y) ~ FALSE))
pus <- pus %>% select(-uID.y) %>% rename(uID=uID.x)

# create dataframe of locked constraints for zones (different format)
# must be numeric value in 'status' field
# MPAs are all in zone 1
locked_df <- pus %>% 
  select(c('uID', 'lockedin')) %>% 
  st_drop_geometry() %>%
  rename(pu=uID) %>%
  filter(lockedin==TRUE)
locked_df$status <- 1
locked_df <- select(locked_df, -lockedin)
locked_df$zone <- 'zone1'

# targets
# need to be in same order as features
targets_csv <- rename(
  targets_csv,
  target_low = `Coarse-filter feature; Low Target Range (10%)`,
  target_med = `Coarse-filter feature; Medium Target Range (20%)`,
  target_hig = `Coarse-filter feature; High Target Range (30%)`)
targets_csv <- select(targets_csv, c(processed_file, target_low, target_med, target_hig))
targets <- left_join(
  as.data.frame(features), 
  targets_csv, 
  by=c('features'='processed_file'), 
  keep=TRUE)
# percent to proportion
targets <- mutate_at(targets, c('target_low', 'target_med', 'target_hig'), function(x)(x/100))





########################################
# function to evaluate each solution
########################################

evaluate_solution <- function(p1, s1) {
  
  # get solution columns for the first solution
  # this accounts for multiple zones
  sols <- grep('solution_1', colnames(s1), value=TRUE)
  
  # evaluate the performance of the first solution
  eval_numsel <- eval_n_summary(p1, s1[,sols])
  eval_cost <- eval_cost_summary(p1, s1[,sols])
  eval_targets <- eval_target_coverage_summary(p1, s1[,sols])
  eval_boundary <- eval_boundary_summary(p1, s1[,sols])
  eval_feat <- eval_feature_representation_summary(p1, s1[,sols])
  
  # set up table
  s2 <- tibble::rowid_to_column(s1, 'id_index')
  
  # Format for output
  # If a solution has zones then it the zones column is in list type since a
  # target can correspond to multiple zones
  # Place zone id in new column
  if("zone" %in% colnames(eval_targets))
  {
    eval_targets$zoning <- vapply(eval_targets$zone, FUN.VALUE = character(1), paste, sep = " & ")
    eval_targets <- select(eval_targets, -zone)
  }
  
  # output solutions, target evaluation, and summary evaluation to csv
  s2 <- as_tibble(s2)
  s2 <- select(s2, -'Shape')
  write.csv(s2, 'solution.csv', row.names=FALSE)
  write.csv(eval_targets, 'eval_targets.csv', row.names=FALSE)
  write.csv(eval_boundary, 'eval_boundary.csv', row.names=FALSE)
  write.csv(eval_feat, 'eval_featurerep.csv', row.names=FALSE)
  df_summ <- tibble(zone=eval_numsel$summary, total_selected=eval_numsel$n, cost=eval_cost$cost)
  write.csv(df_summ, 'eval_summary.csv', row.names=FALSE)
  
}



##############################################################
# solution 201:
# 
# 4 zones
# min_shortfall_objective to identify the ones that fall short
# However, it will spend budget to exceed targets, so it will
# look different than min_set_objective and we can't compare
# it to Marxan scenarios.
# See Jeff’s Jan 25 answer here: 
# https://github.com/prioritizr/prioritizr/issues/224
# The thing on the shopping list and his point 2.
##############################################################

out_folder <- 's201_minshortfall'
out_dir <- file.path(out_path, out_folder)
dir.create(out_dir)
setwd(out_dir)

# zone targets matrix
zone_names = c('zone1', 'zone2', 'zone3', 'zone4')
tz <- matrix(NA, nrow=length(features), ncol=4, dimnames=list(features,zone_names))
for(feat in features){
  t <- targets[targets$features==feat,'target_low']
  z <- zoning[zoning$feature==feat, 'zone_20220315_4zones'][[1]]
  tz[feat, z] <- t
}
tz <- tz %>% replace(is.na(.), 0.0) # features are only targeted in one zone

# Create zones object
z <- zones(
  features, features, features, features,
  zone_names=zone_names,
  feature_names=features
)

# total allowable cost for min_shortfall_objective
total_cost <- sum(pus$COST) + 1000

# A note on solvers:
# When I initially built everything I was able to use the Gurobi solver, which
# performed well in every situation. We now need to use the 3 opensource
# solvers. From testing I found that solve time between the 3 can be wildly 
# different with just small changes to the problem. I would suggest testing
# a different solver is one is taking too long.
# https://prioritizr.net/articles/solver_benchmarks.html
# Also for testing where you don't even care about the solution, raise the gap
# to something very high like 90%. For official solutions, I would suggest 10%.
# With Gurobi we were able to test down to 0%, but that would probably take
# forever with these solvers. Remember, with Marxan we don't have any idea how
# close to optimality we are, so for this evaulation project, its probably more
# important to be consistent with prioritizr than to have everything go to 0%.
p <- problem(pus, z, cost_column=c('COST', 'COST', 'COST', 'COST')) %>%
  add_min_shortfall_objective(total_cost) %>%
  add_relative_targets(tz) %>%
  add_binary_decisions() %>%
  add_cbc_solver(gap = 0.1, threads = parallel::detectCores(TRUE)-1)
  #add_highs_solver(gap=0.1, threads = parallel::detectCores(TRUE)-1)
  #add_lpsymphony_solver(gap=0.1)

# Note: sink() doesn't save cbc solver output. Not sure why. I will record time 
# with tictoc, but you may need to record the solution gap manually.
tic()
sink('log.txt', split=TRUE)
s <- solve(p, force=TRUE)
sink()
time <- toc(log=TRUE)
write(time$callback_msg, file='time.txt')
saveRDS(s, glue('{out_folder}.rds'))
evaluate_solution(p, s)



##############################################################
# solution 202:
# 
# min_set_objective
# adjust targets of ones that were not met in previous solution
##############################################################

out_folder <- 's202_minset_adjtarg'
out_dir <- file.path(out_path, out_folder)
dir.create(out_dir)
setwd(out_dir)

# zone targets matrix
zone_names = c('zone1', 'zone2', 'zone3', 'zone4')
tz <- matrix(NA, nrow=length(features), ncol=4, dimnames=list(features,zone_names))
for(feat in features){
  t <- targets[targets$features==feat,'target_low']
  z <- zoning[zoning$feature==feat, 'zone_20220315_4zones'][[1]]
  tz[feat, z] <- t
}
tz <- tz %>% replace(is.na(.), 0.0) # features are only targeted in one zone

# adjust targets manually based on relative_shortfall in eval_targets.csv from previous solution
tz2 <- tz
tz2['hu_rf_fishing_groundfish',4]<-0.64
tz2['hu_tr_vesseltraffic_d',3] <- 0.64
# tz2['hu_co_fishing_dive_d',4] <- 0.49
# tz2['hu_ot_underwaterinfrastructure',3]<-0.62
# tz2['hu_ot_dredgingsites',2] <- 0.0
# tz2['hu_tr_portsandterminals',2]<-0.64
# tz2['hu_rf_fishing_crab',4]<-0.64
# tz2['hu_ot_citypopulation_d',2]<-0.65
# tz2['hu_rf_fishing_prawnandshrimp',4]<-0.68
# tz2['hu_ot_floatingstructures',2]<-0.69
# tz2['eco_birds_hornedpuffin_colonies',1] <- 0.36
# tz2['hu_tr_anchoragesrecreational',1] <- 0.69
# tz2['hu_ot_loghandlingstorage',2]<-0.69
# tz2['eco_fish_sixgillshark',1]<-0.39
# tz2['hu_co_fishing_seine_salmon_d',4]<-0.69
# tz2['eco_mammals_seaotter_habitat',1]<-0.39
# tz2['eco_fish_salmon',1]<-0.39

# Create zones object
z <- zones(
  features, features, features, features,
  zone_names=zone_names,
  feature_names=features
)

p <- problem(pus, z, cost_column=c('COST', 'COST', 'COST', 'COST')) %>%
  add_min_set_objective() %>%
  add_relative_targets(tz2) %>%
  add_binary_decisions() %>%
  #add_cbc_solver(gap = 0, threads = parallel::detectCores(TRUE)-1)
  add_highs_solver(gap=0.1, threads = parallel::detectCores(TRUE)-1)
  #add_lpsymphony_solver(gap=0.1)

tic()
sink('log.txt', split=TRUE)
s <- solve(p, force=TRUE)
sink()
time <- toc(log=TRUE)
write(time$callback_msg, file='time.txt')
saveRDS(s, glue('{out_folder}.rds'))
evaluate_solution(p, s)




##############################################################
# FROM HERE FORWARD:
# doing all of the below with minshortfall. Once you have a setup, you would
# then want to adjust targets for minsetobjective as I did above.
##############################################################



##############################################################
# solution 203:
# 
# Lock in MPAs
##############################################################

out_folder <- 's203_lockedin'
out_dir <- file.path(out_path, out_folder)
dir.create(out_dir)
setwd(out_dir)

# zone targets matrix
zone_names = c('zone1', 'zone2', 'zone3', 'zone4')
tz <- matrix(NA, nrow=length(features), ncol=4, dimnames=list(features,zone_names))
for(feat in features){
  t <- targets[targets$features==feat,'target_low']
  z <- zoning[zoning$feature==feat, 'zone_20220315_4zones'][[1]]
  tz[feat, z] <- t
}
tz <- tz %>% replace(is.na(.), 0.0) # features are only targeted in one zone

# Create zones object
z <- zones(
  features, features, features, features,
  zone_names=zone_names,
  feature_names=features
)

# set the cost to zero for pus already covered by MPAs
pus$COST[pus$lockedin == TRUE] <- 0

# total allowable cost for min_shortfall_objective
total_cost <- sum(pus$COST) + 1000

p <- problem(pus, z, cost_column=c('COST', 'COST', 'COST', 'COST')) %>%
  add_min_shortfall_objective(total_cost) %>%
  add_relative_targets(tz) %>%
  add_manual_locked_constraints(locked_df) %>%
  add_binary_decisions() %>%
  add_cbc_solver(gap = 0.1, threads = parallel::detectCores(TRUE)-1)
  #add_highs_solver(gap=0.1, threads = parallel::detectCores(TRUE)-1)
  #add_lpsymphony_solver(gap=0.1)

tic()
sink('log.txt', split=TRUE)
s <- solve(p, force=TRUE)
sink()
time <- toc(log=TRUE)
write(time$callback_msg, file='time.txt')
saveRDS(s, glue('{out_folder}.rds'))
evaluate_solution(p, s)



##############################################################
# add_feature_weights():
#
# This is similar to the Species Penalty Factor in Marxan where you favor the
# representation of one species over another.
#
# Documentation for how to implement is here:
# https://prioritizr.net/reference/add_feature_weights.html
# However, I won't demonstrate it here because it cannot be used with the
# minsetobjectve.
# "Weights can only be applied to problems that have an objective that is budget
# limited."
# Minset in Prioritizr: "The difference between this objective and the Marxan 
# software is that the targets for the features will always be met (and as such 
# it does not use Species Penalty Factors)."
#
##############################################################



##############################################################
# Solution 204:
# Boundary penalty
#
# This is similar to the boundary length modifier in Marxan.
# 
# There are approaches to systematically choose the right boundary penalty
# value. See my work and comments in 01_prioritizr_explore_2022.R (BOUNDARY 
# STUFF header). I found that these approaches didn't really give the best
# value. It seems best to just keep increasing/decreasing the penalty value and 
# then viewing the result each time. Essentially, what I ended up looking for 
# was the point at which a "checkered" pattern was mostly eliminated from the 
# solution so that you have contiguous zones.
#
# This takes a LONG time to solve with the open source solvers (10 hours!), and
# it only really made progress with the High solver. I'm really not sure what to
# do here. Perhaps our takeaway for the report is that without the Gurobi solver
# then repeat testing will be difficult. Otherwise, you will need a separate
# desktop computer that you can run problems on. I guess 10 hours isn't the end
# of the world if you have a good starting point for boundary penalty values.
#
# In this problem, I also demonstrate how to read in a previous solution that
# the solver uses as a starting point. This will be helfpul for testing multiple
# boundary penalties and reading in the last one tested.
# (However, that only works with the cbc solver).
#
##############################################################

out_folder <- 's204_boundary1'
out_dir <- file.path(out_path, out_folder)
dir.create(out_dir)
setwd(out_dir)

# zone targets matrix
zone_names = c('zone1', 'zone2', 'zone3', 'zone4')
tz <- matrix(NA, nrow=length(features), ncol=4, dimnames=list(features,zone_names))
for(feat in features){
  t <- targets[targets$features==feat,'target_low']
  z <- zoning[zoning$feature==feat, 'zone_20220315_4zones'][[1]]
  tz[feat, z] <- t
}
tz <- tz %>% replace(is.na(.), 0.0) # features are only targeted in one zone

# Create zones object
z <- zones(
  features, features, features, features,
  zone_names=zone_names,
  feature_names=features
)

# total allowable cost for min_shortfall_objective
total_cost <- sum(pus$COST) + 1000

# Get boundary length data for all pus
pus_bd <- boundary_matrix(pus)
# THE boundary_matrix output has changed slightly since I last used Prioritzr.
# The diagonal is the total perimeter of the planning unit. This is 4000.
# The off-diagonal is the amount shared with another cell, which will be 0 or 
# 1000.

# Re-scale boundary length data to match order of magnitude of costs to avoid
# numerical issues and reduce run times.
#pus_bd@x <- rescale(pus_bd@x, to=c(10,40))
pus_bd <- rescale_matrix(pus_bd, max=40)

# create zone matrix which favors clumping planning units that are
# allocated to the same zone together - this is the default
zb <- diag(4)

# read in previous solution as start solution
pr <- read_rds(file.path(root, 'scripts/dst_02_prioritizr/outputs/s201_minshortfall/s201_minshortfall.rds'))
sols <- grep('solution_1', colnames(pr), value=TRUE)

p <- problem(pus, z, cost_column=c('COST', 'COST', 'COST', 'COST')) %>%
  add_min_shortfall_objective(total_cost) %>%
  add_relative_targets(tz) %>%
  add_binary_decisions() %>%
  add_boundary_penalties(penalty=0.000001, data=pus_bd, zones=zb, edge_factor=c(0.5, 0.5, 0.5, 0.5)) %>%
  #add_cbc_solver(gap = 0.9, threads = parallel::detectCores(TRUE)-1, start_solution = pr[,sols])
  add_highs_solver(gap=0.9, threads = parallel::detectCores(TRUE)-1)
  #add_lpsymphony_solver(gap=0.9)

tic()
sink('log.txt', split=TRUE)
s <- solve(p, force=TRUE)
sink()
time <- toc(log=TRUE)
write(time$callback_msg, file='time.txt')
saveRDS(s, glue('{out_folder}.rds'))
evaluate_solution(p, s)



##############################################################
# solution 205:
# 
# 2 zones
##############################################################

out_folder <- 's205_2zones'
out_dir <- file.path(out_path, out_folder)
dir.create(out_dir)
setwd(out_dir)

# zone targets matrix
zone_names = c('zone1', 'zone2')
tz <- matrix(NA, nrow=length(features), ncol=2, dimnames=list(features,zone_names))
for(feat in features){
  t <- targets[targets$features==feat,'target_low']
  z <- zoning[zoning$feature==feat, 'zone_20220315_2zones'][[1]]
  tz[feat, z] <- t
}
tz <- tz %>% replace(is.na(.), 0.0) # features are only targeted in one zone

# Create zones object
z <- zones(
  features, features,
  zone_names=zone_names,
  feature_names=features
)

# total allowable cost for min_shortfall_objective
total_cost <- sum(pus$COST) + 1000

p <- problem(pus, z, cost_column=c('COST', 'COST')) %>%
  add_min_shortfall_objective(total_cost) %>%
  add_relative_targets(tz) %>%
  add_binary_decisions() %>%
  add_cbc_solver(gap = 0.1, threads = parallel::detectCores(TRUE)-1)
  #add_highs_solver(gap=0.1, threads = parallel::detectCores(TRUE)-1)
  #add_lpsymphony_solver(gap=0.1)

tic()
sink('log.txt', split=TRUE)
s <- solve(p, force=TRUE)
sink()
time <- toc(log=TRUE)
write(time$callback_msg, file='time.txt')
saveRDS(s, glue('{out_folder}.rds'))
evaluate_solution(p, s)


##############################################################
# solution 206:
# 
# no zones
#
# Do with minset objective. If you use minshortfall and there
# are not constraints, then it just spends all of the budget to
# protect every cell.
##############################################################

out_folder <- 's206_nozones'
out_dir <- file.path(out_path, out_folder)
dir.create(out_dir)
setwd(out_dir)

# features
features_eco <- features[startsWith(features, 'eco_')]

# targets
targets_eco <- targets[startsWith(targets$features, 'eco'), ]

p <- problem(pus, features_eco, cost_column='COST') %>%
  add_min_set_objective() %>%
  add_relative_targets(targets_eco$target_low) %>%
  add_binary_decisions() %>%
  #add_cbc_solver(gap = 0.1, threads = parallel::detectCores(TRUE)-1)
  add_highs_solver(gap=0.1, threads = parallel::detectCores(TRUE)-1)
  #add_lpsymphony_solver(gap=0.1)

tic()
sink('log.txt', split=TRUE)
s <- solve(p, force=TRUE)
sink()
time <- toc(log=TRUE)
write(time$callback_msg, file='time.txt')
saveRDS(s, glue('{out_folder}.rds'))
evaluate_solution(p, s)


##############################################################
#
# no zones
# add_linear_penalties()
#
# (won't demonstrate here since you probably won't do it)
#
# In a nozone problem, this would favor selecting planning units that do
# not have human activity data in them - somewhat similar to a reverse marxan.
#
# Documentation: https://prioritizr.net/reference/add_linear_penalties.html
#
# I used this ages ago during my PhD in a project that I ended up dropping. See
# an example here:
# https://github.com/jcristia/connectivity_impacts_prioritize/blob/main/scripts/prioritizr_seagrass/04_runs_20210405.R#L102
# In this case, the presence values were columns in the planning units
# dataframe.
# 
##############################################################



