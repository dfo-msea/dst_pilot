# An evaluation of decision support tools for marine spatial planning in the Southern Shelf Bioregion

__Main author:__  John Cristiani  
__Contributors:__ Beatrice Proudfoot, Carrie Robb  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Pacific Biological Station  
__Contact:__      e-mail: john.cristiani@dfo-mpo.gc.ca


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective

## Summary

## Status

## Contents

## Methods

## Requirements

## Caveats

## Uncertainty

## Acknowledgements

## References

